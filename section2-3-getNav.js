import fetch from 'node-fetch';
import cheerio from 'cheerio';

async function getNav() {

    const input = process.argv[2]
    const opts = {
        headers: {
            cookie: 'hasCookie=true'
        }
    };

    return await fetch('https://codequiz.azurewebsites.net/',opts).then(res => res.text()).then((response) =>{
       
        const $ =  cheerio.load(response);
        const tds =  $('td') 

        for (let i = 0; i < $(tds).length; i++) {
             const td = $($(tds)[i]).text()

             if(td == input) {
                let nav = $(tds)[i+1]
                return  $(nav).text()
            }
        }

        return  "Data not found"
        
    })

    
}
console.log(await getNav())

